package tests;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class informacoesLoginTest {

	@Test
	public void testAdicionarUmaInformacaoAdicionalDoUsuario() {
		System.out.println(System.setProperty("webdriver.chrome.driver", "C:\\Users\\Fecomercio\\Desktop\\chromedriver.exe"));
		
		WebDriver navegador = new ChromeDriver();
		
		//abrindo navegador url desejada
		navegador.get("https://ead.ucsal.br/");
		
		//clicar no botao "acesse aqui"
		navegador.findElement(By.linkText("Acesse aqui!")).click();
		
		//Digitar no campo com id: username o texto "seu usuario"
		navegador.findElement(By.id("username")).sendKeys("000944963");
		
		//Digitar no campo com id: password o texto "sua senha"
		navegador.findElement(By.id("password")).sendKeys("rai7485");
		
		//Clicar no link que possui o id "loginbtn"
		
		navegador.findElement(By.id("loginbtn")).click();
		
		//Validar que dentro sdo elemento com id: "instance-2301-header" h� o texto "vis�o geral dos curcos"
		WebElement vg = navegador.findElement(By.id("instance-1828-header"));
		String textoNoElementoVg = vg.getText();
				
		Assert.assertEquals("Vis�o geral dos cursos", textoNoElementoVg);
				
		//Fechar browser
		navegador.quit();
		
	}
	
}
